package router

import (
	"log"

	"github.com/gin-gonic/gin"

	"watermelon-web-games/controller/api"
)

func InitializeRouter() (router *gin.Engine) {
	router = gin.Default()
	router.LoadHTMLFiles("static/test.html")
	snakeRouter := router.Group("/snake")
	{
		snakeRouter.GET("/room/:roomId", func(c *gin.Context) {
			if pusher := c.Writer.Pusher(); pusher != nil {
				// use pusher.Push() to do server push
				if err := pusher.Push("/static/script/script.js", nil); err != nil {
					log.Printf("Failed to push: %v", err)
				}
			}
			c.HTML(200, "test.html", nil)
		})

		snakeRouter.GET("/ws/:roomId", api.PlaySnake)
	}

	return router
}
