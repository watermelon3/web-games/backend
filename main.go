package main

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"watermelon-web-games/controller/api"
	"watermelon-web-games/router"
)

func main() {
	api.InitializeSnake()

	router := router.InitializeRouter()

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	s := &http.Server{
		Addr:           fmt.Sprintf(":%s", port),
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	if err := s.ListenAndServe(); err != nil {
		panic("[ERROR] Failed to listen and serve")
	}
}
