package constants

import (
	"time"
)

const (
	WriteWait      = 10 * time.Second
	PongWait       = 60 * time.Second
	PingPeriod     = (PongWait * 9) / 10
	MaxMessageSize = 512

	BoardWidth   = 25
	BoardHeight  = 25
	GameInterval = 100 * time.Millisecond
)
