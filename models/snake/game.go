package snake

import (
	"encoding/json"
	"math/rand"
	"time"
	"watermelon-web-games/constants"
)

type Game struct {
	Player1 *player    `json:"player1"`
	Player2 *player    `json:"player2"`
	Food    coordinate `json:"food"`
	Width   int        `json:"width"`
	Height  int        `json:"height"`
	room    *Room      `json:"-"`
	game    chan bool  `json:"-"`
}

type player struct {
	Position coordinate   `json:"head"`
	Velocity coordinate   `json:"-"`
	Body     []coordinate `json:"body"`
	Length   int          `json:"length"`
	Score    int          `json:"score"`
}

type coordinate struct {
	X int `json:"x"`
	Y int `json:"y"`
}

// Global

func CreateGame(width int, height int, room *Room) *Game {
	return &Game{
		Player1: createPlayer(),
		Player2: createPlayer(),
		Width:   width,
		Height:  height,
		room:    room,
	}
}

func createPlayer() *player {
	return &player{
		Score: 0,
	}
}

func seedRandom() {
	rand.Seed(time.Now().UnixNano())
}

func generateRandomCoordinate(boundX int, boundY int) coordinate {
	return coordinate{
		X: rand.Intn(boundX),
		Y: rand.Intn(boundY),
	}
}

func coordinateIsEqual(coor1 coordinate, coor2 coordinate) bool {
	return coor1.X == coor2.X && coor1.Y == coor2.Y
}

func getUpdatedVelocity(direction string) (int, int) {
	switch direction {
	case "up":
		return 0, -1
	case "down":
		return 0, 1
	case "right":
		return 1, 0
	case "left":
		return -1, 0
	default:
		return 0, 0
	}
}

// Game loop

func (game *Game) startGame() {
	game.resetPlayers()
	game.Food = generateRandomCoordinate(game.Width, game.Height)

	game.game = game.startGameLoop()
}

func (game *Game) startGameLoop() chan bool {
	ticker := time.NewTicker(constants.GameInterval)
	done := make(chan bool)

	go func() {
		for {
			select {
			case <-ticker.C:
				game.move()
				game.broadcastToRoom()
			case <-done:
				return
			}
		}
	}()

	return done
}

func (game *Game) resetPlayers() {
	seedRandom()

	game.Player1.Position = generateRandomCoordinate(game.Width, game.Height)
	game.Player2.Position = generateRandomCoordinate(game.Width, game.Height)

	game.Player1.Velocity = coordinate{X: 0, Y: 0}
	game.Player2.Velocity = coordinate{X: 0, Y: 0}

	game.Player1.Body = nil
	game.Player2.Body = nil

	game.Player1.Length = 4
	game.Player2.Length = 4
}

func (game *Game) broadcastToRoom() {
	gameJsonBytes, _ := json.Marshal(game)
	game.room.broadcastToClients(gameJsonBytes)
}

func (game *Game) move() {
	game.movePlayer(game.Player1)
	game.movePlayer(game.Player2)
	game.checkGameOver()
}

func (game *Game) movePlayer(player *player) {
	// Update snake body
	if player.Velocity.X == 0 && player.Velocity.Y == 0 {
		return
	}

	player.Body = append(player.Body, player.Position)

	// Update snake head
	player.Position.X = (player.Position.X + player.Velocity.X + game.Width) % game.Width
	player.Position.Y = (player.Position.Y + player.Velocity.Y + game.Height) % game.Height

	// Check if food eaten
	if coordinateIsEqual(game.Food, player.Position) {
		player.Length += 1
		player.Score += 1
		game.Food = generateRandomCoordinate(game.Width, game.Height)
	}

	// Cut tail
	if len(player.Body) > player.Length {
		player.Body = player.Body[1:]
	}
}

func (game *Game) checkGameOver() {
	collidable := append(game.Player1.Body, game.Player2.Body...)

	for _, checkCoordinate := range collidable {
		if coordinateIsEqual(checkCoordinate, game.Player1.Position) {
			close(game.game)
			game.room.broadcastToClientsString("win two")
			game.Player2.Score += 5
			game.startGame()
		} else if coordinateIsEqual(checkCoordinate, game.Player2.Position) {
			close(game.game)
			game.room.broadcastToClientsString("win one")
			game.Player1.Score += 5
			game.startGame()
		}
	}
}

// Player

func (player *player) changeDirection(direction string) {
	x, y := getUpdatedVelocity(direction)
	if player.Velocity.X == x*-1 && player.Velocity.Y == y*-1 {
		return
	}
	player.Velocity = coordinate{X: x, Y: y}
}
