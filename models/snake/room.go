package snake

import (
	"encoding/json"
	"watermelon-web-games/constants"

	"github.com/google/uuid"
)

type Room struct {
	id         string
	clients    map[uuid.UUID]*Client
	player1    uuid.UUID
	player2    uuid.UUID
	game       *Game
	register   chan *Client
	unregister chan *Client
	broadcast  chan []byte
}

func CreateRoom(id string) *Room {
	room := &Room{
		id:         id,
		clients:    make(map[uuid.UUID]*Client),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		broadcast:  make(chan []byte),
		player1:    uuid.Nil,
		player2:    uuid.Nil,
	}

	room.createGame()
	return room
}

func (room *Room) createGame() {
	game := CreateGame(constants.BoardWidth, constants.BoardHeight, room)
	room.game = game
}

func (room *Room) RunRoom() {
	for {
		select {

		case client := <-room.register:
			room.registerClient(client)

		case client := <-room.unregister:
			room.unregisterClient(client)

		case message := <-room.broadcast:
			room.broadcastToClients(message)
		}
	}
}

func (room *Room) registerClient(client *Client) {
	room.clients[client.id] = client

	if room.player1 == uuid.Nil {
		room.player1 = client.id
		room.broadcastToClientsString("play one")
	} else if room.player2 == uuid.Nil {
		room.player2 = client.id
		room.broadcastToClientsString("play two")
	} else {
		room.broadcastToClientsString("not playing")
	}

	if room.player1 != uuid.Nil && room.player2 != uuid.Nil {
		room.game.startGame()
		room.broadcastToClientsString("start")
	}
}

func (room *Room) unregisterClient(client *Client) {
	delete(room.clients, client.id)

	if room.player1 == client.id {
		room.player1 = uuid.Nil
		room.broadcastToClientsString("left one")
	} else if room.player2 == client.id {
		room.player2 = uuid.Nil
		room.broadcastToClientsString("left two")
	}
}

func (room *Room) broadcastToClientsString(message string) {
	messageJson := &MessageJson{Data: message}
	messageJsonBytes, _ := json.Marshal(messageJson)
	room.broadcast <- []byte(messageJsonBytes)
}

func (room *Room) broadcastToClients(message []byte) {
	for _, client := range room.clients {
		client.send <- message
	}
}

func (room *Room) GetId() string {
	return room.id
}

func (room *Room) handleMessage(message *Message) {
	var messageString = string(message.Data)
	var player *player = nil

	if message.Sender.id == room.player1 {
		player = room.game.Player1
	} else if message.Sender.id == room.player2 {
		player = room.game.Player2
	}

	if player == nil {
		return
	}

	player.changeDirection(messageString)
}
