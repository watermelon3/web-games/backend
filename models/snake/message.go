package snake

type Message struct {
	Data   []byte
	Sender *Client
}

type MessageJson struct {
	Data string `json:"data"`
}
