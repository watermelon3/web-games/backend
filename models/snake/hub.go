package snake

import "github.com/gorilla/websocket"

// hub maintains the set of active connections and broadcasts messages to the
// connections.
type Hub struct {
	rooms      map[string]*Room
	register   chan *Client
	unregister chan *Client
	broadcast  chan *Message
}

func AddNewClient(conn *websocket.Conn, hub *Hub, room string) {
	c := createClient(conn, hub, room)
	hub.register <- c

	go c.readPump()
	go c.writePump()
}

func CreateHub() *Hub {
	return &Hub{
		rooms:      make(map[string]*Room),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		broadcast:  make(chan *Message),
	}
}

func (hub *Hub) Run() {
	for {
		select {
		case client := <-hub.register:
			hub.registerClient(client)

		case client := <-hub.unregister:
			hub.unregisterClient(client)

		case message := <-hub.broadcast:
			hub.broadcastToRoom(message)
		}

	}
}

func (hub *Hub) registerClient(client *Client) {
	room := hub.findRoom(client.room)
	if room == nil {
		room = hub.createRoom(client.room)
	}
	room.registerClient(client)
}

func (hub *Hub) unregisterClient(client *Client) {
	hub.findRoom(client.room).unregisterClient(client)
}

func (hub *Hub) broadcastToRoom(message *Message) {
	hub.findRoom(message.Sender.room).handleMessage(message)
}

func (hub *Hub) findRoom(id string) *Room {
	return hub.rooms[id]
}

func (hub *Hub) createRoom(id string) *Room {
	room := CreateRoom(id)
	go room.RunRoom()
	hub.rooms[id] = room

	return room
}
