package snake

import (
	"log"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"

	"watermelon-web-games/constants"
)

type Client struct {
	id   uuid.UUID
	conn *websocket.Conn
	room string
	hub  *Hub
	send chan []byte
}

func createClient(conn *websocket.Conn, hub *Hub, room string) *Client {
	return &Client{
		id:   uuid.New(),
		conn: conn,
		hub:  hub,
		room: room,
		send: make(chan []byte),
	}
}

func (c *Client) write(mt int, payload []byte) error {
	c.conn.SetWriteDeadline(time.Now().Add(constants.WriteWait))
	return c.conn.WriteMessage(mt, payload)
}

func (c Client) readPump() {
	defer func() {
		c.disconnect()
	}()

	c.conn.SetReadLimit(constants.MaxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(constants.PongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(constants.PongWait)); return nil })

	for {
		_, msg, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
				log.Printf("error: %v", err)
			}
			break
		}

		c.handleMessage(msg)
	}
}

func (c Client) writePump() {
	ticker := time.NewTicker(constants.PingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()

	for {
		select {
		case message, ok := <-c.send:
			if !ok {
				c.write(websocket.CloseMessage, []byte{})
				return
			}
			if err := c.write(websocket.TextMessage, message); err != nil {
				return
			}
		case <-ticker.C:
			if err := c.write(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}

func (client *Client) disconnect() {
	client.hub.unregister <- client
	client.conn.Close()
	close(client.send)
}

func (client *Client) handleMessage(msg []byte) {
	message := &Message{
		Data:   msg,
		Sender: client,
	}

	client.hub.broadcastToRoom(message)
}
