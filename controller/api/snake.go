package api

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"

	"watermelon-web-games/models/snake"
	"watermelon-web-games/utils"
)

var h = snake.CreateHub()

func InitializeSnake() {
	go h.Run()
}

func serveWebsocket(w http.ResponseWriter, r *http.Request, roomId string) {
	ws, err := utils.Upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err.Error())
		return
	}
	snake.AddNewClient(ws, h, roomId)
}

func PlaySnake(c *gin.Context) {
	roomId := c.Param("roomId")
	serveWebsocket(c.Writer, c.Request, roomId)
}
